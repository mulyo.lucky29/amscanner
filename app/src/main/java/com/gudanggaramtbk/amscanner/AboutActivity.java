package com.gudanggaramtbk.amscanner;

import android.app.ActionBar;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class AboutActivity extends AppCompatActivity {
    private DownloadUpdate updaterApk;
    private SharedPreferences config;
    private Button CmdUpdateApk;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        config = getSharedPreferences("AMSCannerSetting", MODE_PRIVATE);
        // initialize updater apk
        updaterApk = new DownloadUpdate(config);
        updaterApk.setContext(this);

        CmdUpdateApk = (Button)findViewById(R.id.cmd_updateAPK);
        CmdUpdateApk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updaterApk.DownloadApk();
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        startActivity(intent);
    }

}
