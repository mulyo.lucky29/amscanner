package com.gudanggaramtbk.amscanner;

/**
 * Created by luckym on 5/28/2017.
 */
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.CharArrayBuffer;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

public class MySQLiteHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "AssetDB.db";
    private static final String TABLE_ASSET = "AMSC_Asset";
    private static final String KEY_ID = "id";
    private static final String KEY_RoomCode = "RoomCode";
    private static final String KEY_EmployeeCode = "EmployeeCode";
    private static final String KEY_AssetCode = "AssetCode";
    private static final String KEY_DeviceID = "DeviceID";
    private static final String KEY_CreationDate = "CreationDate";
    private static final String KEY_Flag = "Flag";
    private static final String KEY_Qty = "Qty";

    private static final String KEY_ORDER_BY = KEY_CreationDate;

    private static final String TAG = "MyActivity";
    private static final String[] COLUMNS = {KEY_ID,KEY_RoomCode,KEY_EmployeeCode,KEY_AssetCode,KEY_DeviceID,KEY_CreationDate,KEY_Qty};

    private static Cursor XCursor;

    public static final String CREATE_ASSET_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_ASSET + " ( " +
                                                                        KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                                                                        KEY_RoomCode + " TEXT, " +
                                                                        KEY_EmployeeCode + " TEXT, " +
                                                                        KEY_AssetCode + " TEXT, " +
                                                                        KEY_DeviceID + " TEXT," +
                                                                        KEY_CreationDate + " TEXT," +
                                                                        KEY_Flag  + " TEXT,"  +
                                                                        KEY_Qty + " TEXT); ";

    public MySQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_ASSET_TABLE);
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older books table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ASSET);
        onCreate(db);
    }

    public Cursor GetCursor(){
        return XCursor;
    }

    //---------------------------------------------------------------------

    public long addAsset(String  SRoomCode,
                         String  SEmployeeCode,
                         String  SAssetCode,
                         String  SDeviceID,
                         String  SQty
                         ){
        long savestatus;
        String sysdate = new SimpleDateFormat("dd-MMM-yy hh:mm:ss a").format(new Date());

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_RoomCode, SRoomCode);
        values.put(KEY_EmployeeCode, SEmployeeCode);
        values.put(KEY_AssetCode, SAssetCode);
        values.put(KEY_DeviceID, SDeviceID);
        values.put(KEY_CreationDate, sysdate);
        values.put(KEY_Qty,SQty);

        savestatus = db.insert(TABLE_ASSET,null,values); // key/value -> keys = column names/ values = column values
        db.close();
        return savestatus;
    }

    public long delAsset(String XPID){
        long deletestatus;
        SQLiteDatabase db = this.getWritableDatabase();
        deletestatus = db.delete(TABLE_ASSET,"ID = " + XPID,null);
        db.close();
        return deletestatus;
    }

    public long editAsset(String P_Asset_Code,
                          String P_Employee_Code,
                          String P_Room_Code,
                          String P_Qty,
                          String PID){
        long editstatus;

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues args = new ContentValues();
        args.put(KEY_AssetCode, P_Asset_Code);
        args.put(KEY_EmployeeCode, P_Employee_Code);
        args.put(KEY_RoomCode, P_Room_Code);
        args.put(KEY_Qty, P_Qty);

        editstatus = db.update(TABLE_ASSET, args, KEY_ID + "=" + PID, null);

        db.close();
        return editstatus;
    }

    public int getCount() {
        try {
            SQLiteDatabase db =  this.getWritableDatabase();
            String count = "SELECT count(*) FROM " + TABLE_ASSET;
            Cursor mcursor = db.rawQuery(count, null);
            mcursor.moveToFirst();
            int icount = mcursor.getInt(0);
            if(icount>0){
                return icount;
            }
            else{
                return 0;
            }
        }
        catch(SQLiteException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public List<Scan> getAllItems()
    {
        List<Scan> items = new ArrayList<Scan>();
        SQLiteDatabase db = this.getWritableDatabase();
        XCursor = db.query(MySQLiteHelper.TABLE_ASSET,
                                COLUMNS,
                                null,
                                null,
                                null,
                                null ,
                                null);

        XCursor.moveToFirst();
        while (!XCursor.isAfterLast())
        {
            Scan cursorItem = cursorToItem(XCursor);
            items.add(cursorItem);
            XCursor.moveToNext();
        }
        //XCursor.close();
        db.close();
        return items;
    }

    public Scan cursorToItem(Cursor cursor)
    {
        Scan ix = new Scan();
        //ix.setID(cursor.getInt(0));
        //{KEY_ID,KEY_RoomCode,KEY_EmployeeCode,KEY_AssetCode,KEY_DeviceID,KEY_CreationDate,KEY_Qty};

        ix.setID(cursor.getString(0));
        ix.setRoomCode(cursor.getString(1));
        ix.setEmployeeCode(cursor.getString(2));
        ix.setAssetCode(cursor.getString(3));
        ix.setDeviceID(cursor.getString(4));
        ix.setScanDate(cursor.getString(5));
        ix.setQty(cursor.getString(6));

        return ix;
    }

}