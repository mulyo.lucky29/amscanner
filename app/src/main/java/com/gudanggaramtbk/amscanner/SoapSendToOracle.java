package com.gudanggaramtbk.amscanner;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.util.Log;
import android.widget.Toast;
import android.app.Activity;

/**
 * Created by LuckyM on 6/12/2017.
 */

public class SoapSendToOracle {
    private SharedPreferences config;
    private Context context;
    private MySQLiteHelper dbHelper;
    private MainActivity pActivity;

    // override constructor
    public SoapSendToOracle(SharedPreferences PConfig){
        Log.d("[GudangGaram]: ", "SoapSendToOracle Constructor");
        this.config = PConfig;
    }
    public void setContext(Context context){
        Log.d("[GudangGaram]: ", "SetContext");
        this.context = context;
    }
    public void setParentActivity(MainActivity pActivity){
        this.pActivity = pActivity;
    }

    public void setDBHelper(MySQLiteHelper dbHelper){
        Log.d("[GudangGaram]: ", "SetDBHelper");
        this.dbHelper = dbHelper;
    }
    public void SentToOracle() {
        Cursor XCur;
        MainActivity mx = new MainActivity();

        Log.d("[GudangGaram]: ", "SentToOracle Begin");
        XCur = dbHelper.GetCursor();
            if(XCur.getCount() > 0) {
                try {
                    XCur.moveToFirst();
                    while (!XCur.isAfterLast()) {
                        Scan cursorItem = dbHelper.cursorToItem(XCur);

                        String WSP_ID = cursorItem.getID();
                        String WSP_Employee_code = cursorItem.getEmployeeCode();
                        String WSP_Asset_code = cursorItem.getAssetCode();
                        String WSP_Room_code = cursorItem.getRoomCode();
                        String WSP_Device_ID = cursorItem.getDeviceID();
                        String WSP_ScanDate = cursorItem.getScanDate();
                        String WSP_Qty = cursorItem.getQty();

                        Log.d("[GudangGaram]: ", "Asset Code " + WSP_Asset_code);
                        Log.d("[GudangGaram]: ", "Scan Date " + WSP_ScanDate);

                        // call web service
                        //SoapAccessTaskAsync SoapRequest = new SoapAccessTaskAsync(context,dbHelper,NAMESPACE, SOAP_ADDRESS, OPERATION_NAME);
                        //SoapRequest.execute(WSP_ID,WSP_Room_code, WSP_Employee_code, WSP_Asset_code, WSP_Device_ID);

                        SoapAccessTaskAsync SoapRequest = new SoapAccessTaskAsync(new SoapAccessTaskAsync.SoapAccessTaskAsyncResponse() {
                            @Override
                            public void PostSentAction(String output) {
                                Log.d("[GudangGaram]: ", "PostSentAction Output : " + output);
                            }
                        });
                        SoapRequest.setAttribute(context, dbHelper, config);
                        SoapRequest.execute(WSP_ID, WSP_Room_code, WSP_Employee_code, WSP_Asset_code, WSP_Device_ID,WSP_Qty,WSP_ScanDate);

                        XCur.moveToNext();
                    }
                }
                catch(Exception e){
                    e.printStackTrace();
                }
                finally{
                    XCur.close();
                    Log.d("[GudangGaram]: ", "SentToOracle End");
                    //Snackbar.make(XView, "Data Sent To Oracle :", Snackbar.LENGTH_LONG).show();
                }
                //mx.EH_refresh();
                //pActivity.EH_cmd_Refresh();
            }
            else {
                Toast.makeText(context, "Nothing To Transfer", Toast.LENGTH_LONG).show();
            }
    }


}
