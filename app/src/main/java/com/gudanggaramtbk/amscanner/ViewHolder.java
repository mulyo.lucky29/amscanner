package com.gudanggaramtbk.amscanner;

import android.widget.TextView;

/**
 * Created by LuckyM on 6/5/2017.
 */

public class ViewHolder {
    public TextView XEmployeeCode;
    public TextView XRoomCode;
    public TextView XAssetCode;
    public TextView XQty;
    public TextView XScanDate;
}
