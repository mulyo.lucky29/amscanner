package com.gudanggaramtbk.amscanner;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import android.os.StrictMode;



public class MainActivity extends AppCompatActivity
                          implements NavigationView.OnNavigationItemSelectedListener {
    public static final int SCAN_CODE_ROOM = 1;

    private MySQLiteHelper dbHelper;

    private SoapSendToOracle senderSoap;
    private SharedPreferences config;

    private TextView SDeviceID;
    private List<Scan> lvi;
    private ListView list1;
    private CustomListAdapter adapter;
    private Integer LPosition;
    private AlertDialog.Builder alertDialogBuilder;
    private AlertDialog alertDialog;


    //////////////////////////////////////////////////// OVERRIDE METHOD ////////////////////////////////////////

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        //if (android.os.Build.VERSION.SDK_INT > 9) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        //}

        config = getSharedPreferences("AMSCannerSetting", MODE_PRIVATE);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EH_cmd_Refresh();
                Snackbar.make(view, "Data Reloaded", Snackbar.LENGTH_LONG).setAction("Action",null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        // -------- navigation bat ------------
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View hView =  navigationView.getHeaderView(0);
        navigationView.setNavigationItemSelectedListener(this);
        TextView nav_device_id = (TextView)hView.findViewById(R.id.txt_xdevice_id);
        nav_device_id.setText(Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID));

        // initialize dbHelper
        dbHelper = new MySQLiteHelper(this);
        dbHelper.getWritableDatabase();

        // initialize soap sender
        senderSoap = new SoapSendToOracle(config);
        senderSoap.setContext(MainActivity.this);
        senderSoap.setDBHelper(dbHelper);

        //SDeviceID.setText(Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID));

        EH_cmd_Refresh();
        Intent IRefresh = getIntent();
        if(IRefresh.getStringExtra("REFRESH") == "Y"){
            EH_cmd_Refresh();
        }
        LPosition = 0;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SCAN_CODE_ROOM && resultCode == RESULT_OK) {

        }
    }
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            EH_cmd_delete(LPosition);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_scan) {
            EH_cmd_scan();
        }
        else if (id == R.id.nav_setting) {
            EH_cmd_setting();
        }
        else if (id == R.id.nav_about) {
            EH_cmd_about();
        }
        else if(id == R.id.nav_exit){
            EH_cmd_exit();
        }
        else if (id == R.id.nav_sync) {
            EH_cmd_SentToOracle();
        }
        else if (id == R.id.nav_refresh){
            EH_cmd_Refresh();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    return true;
    }

    /////////////////////////////////////////////////   EVENT HANDLER ///////////////////////////////////////////

    public void EH_cmd_ok_edit(String P_Asset_Code,
                               String P_Employee_Code,
                               String P_Room_Code,
                               String P_Qty,
                               final String PID,
                               final Integer PPosition){
        if(P_Qty.length() > 0 && PID.length() > 0 ){
            dbHelper.editAsset(P_Asset_Code, P_Employee_Code, P_Room_Code, P_Qty, PID);
            adapter.notifyDataSetChanged();
            EH_cmd_Refresh();
        }
    }

    public void EH_cmd_edit(String P_Asset_Code,
                            String P_Employee_Code,
                            String P_Room_Code,
                            String P_Qty,
                            final String PID,
                            final Integer PPosition){
        // get prompts.xml view
        Button CmdEditClear;

        LayoutInflater li = LayoutInflater.from(this);
        View promptsView = li.inflate(R.layout.editscan, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setTitle("Edit Scanner Data");
        alertDialogBuilder.setView(promptsView);

        final EditText txt_EEmployeeCode = (EditText) promptsView.findViewById(R.id.txt_edit_EmployeeCode);
        final EditText txt_ERoomCode     = (EditText) promptsView.findViewById(R.id.txt_edit_RoomCode);
        final EditText txt_EAssetCode    = (EditText) promptsView.findViewById(R.id.txt_edit_AssetCode);
        final EditText txt_edit_Qty      = (EditText) promptsView.findViewById(R.id.txt_edit_Qty);
        CmdEditClear = (Button) promptsView.findViewById(R.id.cmd_edit_clear);

        txt_EAssetCode.setText(P_Asset_Code);
        txt_EEmployeeCode.setText(P_Employee_Code);
        txt_ERoomCode.setText(P_Room_Code);
        txt_edit_Qty.setText(P_Qty);

        CmdEditClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(getApplicationContext(),"Clear", Toast.LENGTH_LONG).show();
                txt_EEmployeeCode.setText("");
                txt_ERoomCode.setText("");
                txt_EAssetCode.setText("");
                txt_edit_Qty.setText("");
            }
        });



        // set dialog message
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {

                                EH_cmd_ok_edit(txt_EAssetCode.getText().toString(),
                                               txt_EEmployeeCode.getText().toString(),
                                               txt_ERoomCode.getText().toString(),
                                               txt_edit_Qty.getText().toString(),
                                               PID.toString(),
                                               PPosition);
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });
        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        // show it
        alertDialog.show();
    }

    public void EH_cmd_delete(Integer PPosition){
        if(lvi.size() > 0){
            Toast.makeText(getApplicationContext(),"Data Exist", Toast.LENGTH_LONG).show();
            dbHelper.delAsset(lvi.get(PPosition).getID().toString());
            adapter.notifyDataSetChanged();
            EH_cmd_Refresh();
        }
        else {
            Toast.makeText(getApplicationContext(),"Empty", Toast.LENGTH_LONG).show();
        }
    }

    public void EH_cmd_scan(){
        Intent I_ScanAttribute;
        Toast.makeText(getApplicationContext(), "Scan Barcode", Toast.LENGTH_LONG).show();
        I_ScanAttribute = new Intent(this, ScanAttribute.class);
        startActivity(I_ScanAttribute);
    }

    public void EH_cmd_setting(){
        Intent I_Setting;
        Toast.makeText(getApplicationContext(),"Setting", Toast.LENGTH_LONG).show();
        I_Setting = new Intent(this, SettingActivity.class);
        startActivity(I_Setting);
    }

    public void EH_cmd_about(){
        Intent I_About;
        Toast.makeText(getApplicationContext(),"About", Toast.LENGTH_LONG).show();
        I_About = new Intent(this, AboutActivity.class);
        startActivity(I_About);
    }

    public void EH_refresh(){
        Log.d("[GudangGaram]", " Call Refresh ListView");
    }

    public void EH_cmd_Refresh(){
        try
        {
            lvi = dbHelper.getAllItems();
            list1 = (ListView) findViewById(R.id.lst_assetDB);
            adapter = new CustomListAdapter(this,lvi);
            adapter.notifyDataSetChanged();
            list1.setAdapter(adapter);
            list1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    LPosition = position;
                    list1.setSelection(position);
                    //list1.getChildAt(position).setBackgroundColor(Color.LTGRAY);
                    //Toast.makeText(MainActivity.this, "You Clicked at " + position + " value : "  +  lvi.get(position).getID().toString() , Toast.LENGTH_SHORT).show();
                }
            });

            list1.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view,
                                           int position, long id) {
                EH_cmd_edit(lvi.get(position).getAssetCode().toString(),
                        lvi.get(position).getEmployeeCode().toString(),
                        lvi.get(position).getRoomCode().toString(),
                        lvi.get(position).getQty().toString(),
                        lvi.get(position).getID().toString(),
                        position);

                return true;
            }
        });
        }
        catch (Exception e) {
            Log.d("[GudangGaram]", e.getMessage().toString());
            e.printStackTrace();
        }
    }
    public void EH_cmd_SentToOracle(){
        senderSoap.SentToOracle();
    }
    public void EH_cmd_exit(){
        int pid;
        finish();
        pid = android.os.Process.myPid();
        android.os.Process.killProcess(pid);
    }

 }
