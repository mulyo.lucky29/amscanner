package com.gudanggaramtbk.amscanner;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.client.android.Intents;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.util.Timer;
import java.util.TimerTask;

public class ScanAttribute extends AppCompatActivity implements OnClickListener {
    private Button CmdScanRoom,CmdScanEmployee, CmdGoToScanAsset;
    private TextView TxtScanRoom, TxtScanEmployee;
    private String ScanType;
    Intent IScanEmployee,I_Room,I_Employee;
    IntentIntegrator SI_Scan_room, SI_Scan_Employee;
    private SharedPreferences config;
    private Boolean ScanState;
    private Timer timer0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_attribute);

        CmdScanRoom = (Button)findViewById(R.id.cmd_ARoomCode);
        CmdScanEmployee = (Button)findViewById(R.id.cmd_AEmployeeCode);
        CmdGoToScanAsset = (Button)findViewById(R.id.cmd_ANextAsset);

        TxtScanRoom  = (TextView) findViewById(R.id.txt_ARoomCode);
        TxtScanEmployee = (TextView) findViewById(R.id.txt_AEmployeeCode);

        CmdScanRoom.setOnClickListener(this);
        CmdScanEmployee.setOnClickListener(this);
        CmdGoToScanAsset.setOnClickListener(this);

        config = getSharedPreferences("AMSCannerSetting", MODE_PRIVATE);
        ScanState = config.getBoolean("ScanMode",false);

        if(ScanState == true){
            CmdScanRoom.setVisibility(View.VISIBLE);
            CmdScanEmployee.setVisibility(View.VISIBLE);
            CmdGoToScanAsset.setVisibility(View.VISIBLE);
        }
        else{
            CmdScanRoom.setVisibility(View.INVISIBLE);
            CmdScanEmployee.setVisibility(View.INVISIBLE);
            CmdGoToScanAsset.setVisibility(View.VISIBLE);
        }

        ScanType = "ROOM";

        /*
        // mode otomatis ketika Scan Employee di isi -- cuma employee ini nga mandatory
        TxtScanEmployee.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) {
                // set focus to txtScanAsset
                if(ScanState == Boolean.FALSE) {
                    timer0 = new Timer();
                    timer0.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (TxtScanEmployee.getText().length() > 0) {
                                        EH_cmd_Go_ScanAsset();
                                    }
                                }
                            });
                        }

                    }, 600);
                }
            }
        });
        */
    }

    public void EH_cmd_Go_ScanAsset(){
        //Toast.makeText(getApplicationContext(), "Go To Asset", Toast.LENGTH_LONG).show();
        IScanEmployee = new Intent(ScanAttribute.this, ScanAssetActivity.class);
        IScanEmployee.putExtra("ROOM_CODE", TxtScanRoom.getText().toString());
        IScanEmployee.putExtra("EMPLOYEE_CODE", TxtScanEmployee.getText().toString());
        startActivity(IScanEmployee);
    }

    @Override
    public void onClick(View v){
        int SummonID;
        //respond to clicks
        SummonID = v.getId();
        if(SummonID == R.id.cmd_ANextAsset) {
            EH_cmd_Go_ScanAsset();
            // go to next activity
        }
        else if(SummonID == R.id.cmd_ARoomCode){
            TxtScanRoom.setFocusable(true);
            TxtScanRoom.requestFocus();
            ScanType = "ROOM";
            SI_Scan_room = new IntentIntegrator(this);
            SI_Scan_room.setCaptureActivity(CaptureCustomeActivity.class);
            SI_Scan_room.setBeepEnabled(true);
            SI_Scan_room.initiateScan();
        }
        else if(SummonID == R.id.cmd_AEmployeeCode){
            TxtScanEmployee.setFocusable(true);
            TxtScanEmployee.requestFocus();
            ScanType = "EMPLOYEE";
            SI_Scan_Employee = new IntentIntegrator(this);
            SI_Scan_Employee.setCaptureActivity(CaptureCustomeActivity.class);
            SI_Scan_Employee.setBeepEnabled(true);
            SI_Scan_Employee.initiateScan();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        IntentResult SR;
        String StrScanRoom, StrScanEmployee;

        switch(requestCode) {
            case IntentIntegrator.REQUEST_CODE:
            {
                if (resultCode == RESULT_CANCELED){
                }
                else
                {
                    SR = IntentIntegrator.parseActivityResult(requestCode,resultCode,intent);
                    if(SR != null){
                        if(ScanType == "ROOM"){
                            StrScanRoom = SR.getContents();
                            TxtScanRoom.setText(StrScanRoom);
                            // move cursor to scan employee
                            TxtScanEmployee.setFocusable(true);
                            TxtScanEmployee.requestFocus();
                        }
                        else if(ScanType == "EMPLOYEE") {
                            StrScanEmployee = SR.getContents();
                            TxtScanEmployee.setText(StrScanEmployee);
                        }
                    }
                }
                break;
            }
        }
    }


}
