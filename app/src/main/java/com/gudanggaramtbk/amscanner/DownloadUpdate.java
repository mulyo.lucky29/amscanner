package com.gudanggaramtbk.amscanner;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;
import java.io.File;

/**
 * Created by LuckyM on 6/9/2017.
 */

public class DownloadUpdate  {
    private SharedPreferences config;
    private Context context;
    private String apkbaseurl;
    private ProgressDialog pdLoading;

    // override constructor
    public DownloadUpdate(SharedPreferences PConfig){
        this.config = PConfig;
    }
    public void setContext(Context context){
        this.context = context;
    }

    public void InstalApk(){
        Log.d("[GudangGaram]: ", "APK Install");
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.fromFile(new File(Environment.getExternalStorageDirectory().toString() + File.separator + "app-release.apk")), "application/vnd.android.package-archive");
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
        Log.d("[GudangGaram]: ", "APK Install Done");
    }

    public void DownloadApk()
    {
            this.apkbaseurl = config.getString("URLUpdater","");

            DownloadTaskAsync d = new DownloadTaskAsync(new DownloadTaskAsync.DownloadTaskAsyncResponse() {
                @Override
                public void PostDownloadAction(String output) {
                    Log.d("[GudangGaram]: ", "APK Download Status " + output);
                    if(output == "Finish"){
                        InstalApk();
                    }
                    //pdLoading.dismiss();
                }
            });
            d.setContext(context);
            d.execute(apkbaseurl);
    }
}
