package com.gudanggaramtbk.amscanner;

import android.app.Activity;
import android.content.ClipData;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;


/**
 * Created by LuckyM on 6/2/2017.
 */

public class CustomListAdapter extends ArrayAdapter<Scan> {
     //Scan[] modelItems = null;
     Context mcontext;
     List<Scan> scanList;
     private LayoutInflater mInflater;

        public CustomListAdapter(Context context, List<Scan> list)
        {
            super(context,0,list);
            mcontext = context;
            scanList = list;
            mInflater = (LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }


        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if(convertView == null) {
                convertView = mInflater.inflate(R.layout.rowlist, parent, false);
                holder = new ViewHolder();
                holder.XAssetCode = (TextView) convertView.findViewById(R.id.LblRS_AssetCode);
                holder.XEmployeeCode = (TextView) convertView.findViewById(R.id.LblRS_EmployeeCode);
                holder.XRoomCode = (TextView) convertView.findViewById(R.id.LblRS_RoomCode);
                holder.XQty = (TextView) convertView.findViewById(R.id.LblRS_Qty);
                holder.XScanDate = (TextView) convertView.findViewById(R.id.LblRS_ScanDate);

                convertView.setTag(holder);
            }
            else{
                holder = (ViewHolder) convertView.getTag();
            }

            TextView TAssetCode    =  holder.XAssetCode;
            TextView TEmployeeCode =  holder.XEmployeeCode;
            TextView TRoomCode     =  holder.XRoomCode;
            TextView TQty          =  holder.XQty;
            TextView TScanDate     =  holder.XScanDate;

            Scan o = getItem(position);

            TAssetCode.setText("Asset Code : " + o.getAssetCode());
            TEmployeeCode.setText("Employee Code : " + o.getEmployeeCode());
            TRoomCode.setText("Room Code        : " + o.getRoomCode());
            TQty.setText("Qty : " + o.getQty());
            TScanDate.setText("Scan Date           : " + o.getScanDate());

            /*
            if(modelItems[position].getValue() == 1)
                cb.setChecked(true);
            else
                cb.setChecked(false);
            */

            return convertView;
        }

}





