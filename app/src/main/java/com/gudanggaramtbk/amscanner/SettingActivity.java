package com.gudanggaramtbk.amscanner;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.SQLException;
import com.google.zxing.integration.android.IntentIntegrator;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

public class SettingActivity extends AppCompatActivity implements OnClickListener {
    private Button CmdSaveSetting,CmdTestConn;
    private TextView TxtSoapAddress, TxtSoapNamespace, TxtSoapMethod,TxtUrlUpdater;
    private Switch SWScannerMode;

    String SSoapAddress,SSoapMethod,SSoapNamespace, SUrlUpdater;
    Boolean ScannerModeState;

    SharedPreferences config;
    ProgressDialog pd;


    /*
            String SOAP_ADDRESS = "http://10.50.131.18/WSAMScanner/WSAMScanner.asmx";
            String SOAP_ACTION = "http://gudanggaramtbk.com/InsertToOracle";
            String OPERATION_NAME = "InsertToOracle";
            String NAMESPACE = "http://gudanggaramtbk.com/";
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        CmdSaveSetting    = (Button)findViewById(R.id.cmd_saveSetting);
        CmdTestConn       = (Button)findViewById(R.id.cmd_testConn);
        TxtSoapAddress    = (TextView) findViewById(R.id.txt_SettingSOAPAddress);
        TxtSoapNamespace  = (TextView) findViewById(R.id.txt_SettingSOAPNamespace);
        TxtSoapMethod     = (TextView) findViewById(R.id.txt_SettingSOAPMethod);
        TxtUrlUpdater     = (TextView) findViewById(R.id.txt_SettingURLUpdaters);
        SWScannerMode     = (Switch) findViewById(R.id.ScannerMode);


        config = getSharedPreferences("AMSCannerSetting", MODE_PRIVATE);

        CmdSaveSetting.setOnClickListener(this);
        reload_setting(config);
        CmdTestConn.setOnClickListener(this);

    }

    @Override
    public void onClick(View v){
        int SummonID;
        IntentIntegrator SI_Scan_asset;

        //respond to clicks
        SummonID = v.getId();
        if(SummonID == R.id.cmd_saveSetting){
            // go to next activity
            save_setting(config);
            Toast.makeText(getApplicationContext(),"Setting Saved", Toast.LENGTH_LONG).show();
            Toast.makeText(getApplicationContext(), ScannerModeState.toString() , Toast.LENGTH_LONG).show();

            reload_setting(config);
        }
        else if(SummonID == R.id.cmd_testConn) {
            test_connection();
        }
    }

    public String test_connection(){
        String xfactor;
        String Message;
        Message = "Checking Connection ... Please Wait ...";

        xfactor = "S";
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;

        try{
            HttpTransportSE httpTransport = new HttpTransportSE(SSoapAddress);
            httpTransport.debug = true;
            httpTransport.call( SSoapNamespace + SSoapMethod, envelope);
            httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
        }
        catch(Exception ex){
            xfactor = ex.getMessage().toString();
            Toast.makeText(getApplicationContext(), ex.getMessage().toString(), Toast.LENGTH_LONG).show();
            Log.d("[GudangGaram]", "Catch : " + ex.getMessage().toString());
        }
        if(xfactor == "S"){
            Toast.makeText(getApplicationContext(), "Test Connection Succeed ", Toast.LENGTH_LONG).show();
        }
        return xfactor;
    }

    private void save_setting(SharedPreferences xconfig){
        SharedPreferences.Editor editor = xconfig.edit();
        editor.putString("SoapAddress",TxtSoapAddress.getText().toString());
        editor.putString("SoapMethod",TxtSoapMethod.getText().toString());
        editor.putString("SoapNamespace",TxtSoapNamespace.getText().toString());
        editor.putString("URLUpdater",TxtUrlUpdater.getText().toString());
        editor.putBoolean("ScanMode",SWScannerMode.isChecked());
        editor.commit();
    }

    private void reload_setting(SharedPreferences xconfig){
        SSoapAddress = xconfig.getString("SoapAddress", "");
        SSoapMethod = xconfig.getString("SoapMethod", "");
        SSoapNamespace = xconfig.getString("SoapNamespace", "");
        SUrlUpdater = xconfig.getString("URLUpdater","");
        ScannerModeState = xconfig.getBoolean("ScanMode",false);

        TxtSoapNamespace.setText(SSoapNamespace);
        TxtSoapMethod.setText(SSoapMethod);
        TxtSoapAddress.setText(SSoapAddress);
        TxtUrlUpdater.setText(SUrlUpdater);
        SWScannerMode.setChecked(ScannerModeState);

    }

}
