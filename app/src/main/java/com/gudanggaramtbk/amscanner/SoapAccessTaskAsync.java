package com.gudanggaramtbk.amscanner;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

/**
 * Created by LuckyM on 6/12/2017.
 */

public class SoapAccessTaskAsync extends AsyncTask<String, Void, String> {
    private SharedPreferences config;
    private Context context;
    private ProgressDialog pd;
    private MySQLiteHelper dbHelper;
    private String SentResponse;

    public SoapAccessTaskAsync(){

    }

    public interface SoapAccessTaskAsyncResponse {
        void PostSentAction(String output);
    }
    public SoapAccessTaskAsyncResponse delegate = null;
    public SoapAccessTaskAsync(SoapAccessTaskAsyncResponse delegate){
        this.delegate = delegate;
    }
    public void setContext(Context context){
        this.context = context;
    }
    public void setAttribute(Context context,
                             MySQLiteHelper dbHelper,
                             SharedPreferences config){

        Log.d("[GudangGaram]", "SoapAccessTaskAsync :: setAttribute");
        this.context        = context;
        this.dbHelper       = dbHelper;
        this.config         = config;
        this.pd = new ProgressDialog(this.context);
    }

    @Override
    protected void onPreExecute() {
        Log.d("[GudangGaram]", "SoapAccessTaskAsync :: onPreExecute");
        super.onPreExecute();
        pd.setMessage("Sending Data To Oracle ... ");
        pd.setIndeterminate(false);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setCancelable(false);
        pd.show();
    }
    @Override
    protected void onPostExecute(String file_url) {
        pd.dismiss();
        Log.d("[GudangGaram]", "SoapAccessTaskAsync :: onPostExecute >> " + SentResponse);
        delegate.PostSentAction(file_url);
        Toast.makeText(context,"Sent Data Done", Toast.LENGTH_LONG).show();
    }
    @Override
    protected String doInBackground(String... params) {
        int Result;
        int count;

        String NAMESPACE = config.getString("SoapNamespace", "");   //"http://gudanggaramtbk.com/";
        String SOAP_ADDRESS = config.getString("SoapAddress", "");  //"http://10.50.131.18/WSAMScanner/WSAMScanner.asmx";
        String OPERATION_NAME = config.getString("SoapMethod", ""); //"InsertToOracle";
        String SOAP_ACTION = NAMESPACE + OPERATION_NAME;            //"http://gudanggaramtbk.com/InsertToOracle";

        Log.d("[GudangGaram]", "SOAP ADDRESS   :" + SOAP_ADDRESS);

        count = params.length;

        SoapObject request = new SoapObject(NAMESPACE, OPERATION_NAME);

        // param 0
        PropertyInfo prop_ID = new PropertyInfo();
        prop_ID.setName("ID");
        prop_ID.setValue(params[0]);
        prop_ID.setType(String.class);
        request.addProperty(prop_ID);
        Log.d("[GudangGaram]: ", "params[0] : " + params[0].toString());

        // param 1
        PropertyInfo prop_PRoomCode = new PropertyInfo();
        prop_PRoomCode.setName("RoomCode");
        prop_PRoomCode.setValue(params[1]);
        prop_PRoomCode.setType(String.class);
        request.addProperty(prop_PRoomCode);
        Log.d("[GudangGaram]: ", "params[1] : " + params[1].toString());

        // param 2
        PropertyInfo prop_EmployeeCode = new PropertyInfo();
        prop_EmployeeCode.setName("EmployeeCode");
        prop_EmployeeCode.setValue(params[2]);
        prop_EmployeeCode.setType(String.class);
        request.addProperty(prop_EmployeeCode);
        Log.d("[GudangGaram]: ", "params[2] : " + params[2].toString());

        // param 3
        PropertyInfo prop_AssetCode = new PropertyInfo();
        prop_AssetCode.setName("AssetCode");
        prop_AssetCode.setValue(params[3]);
        prop_AssetCode.setType(String.class);
        request.addProperty(prop_AssetCode);
        Log.d("[GudangGaram]: ", "params[3] : " + params[3].toString());

        // param 4
        PropertyInfo prop_DeviceID = new PropertyInfo();
        prop_DeviceID.setName("DeviceID");
        prop_DeviceID.setValue(params[4]);
        prop_DeviceID.setType(String.class);
        request.addProperty(prop_DeviceID);
        Log.d("[GudangGaram]: ", "params[4] : " + params[4].toString());

        // param 5
        PropertyInfo prop_Qty = new PropertyInfo();
        prop_Qty.setName("Qty");
        prop_Qty.setValue(params[5]);
        prop_Qty.setType(String.class);
        request.addProperty(prop_Qty);
        Log.d("[GudangGaram]: ", "params[5] : " + params[5].toString());

        // param 6
        PropertyInfo prop_ScanDate = new PropertyInfo();
        prop_ScanDate.setName("ScanDate");
        prop_ScanDate.setValue(params[6]);
        prop_ScanDate.setType(String.class);
        request.addProperty(prop_ScanDate);
        Log.d("[GudangGaram]: ", "params[6] : " + params[6].toString());

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);

        Log.d("[GudangGaram]", "SoapAccessTaskAsync :: start " + params[3] + " doInBackground");

        String response = null;
        //Web method call
        try {
            HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
            httpTransport.debug = true;
            httpTransport.call(SOAP_ACTION, envelope);
            httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");

            SoapObject result = (SoapObject) envelope.bodyIn;
            String TResult = result.getProperty(0).toString();
            Log.d("[GudangGaram]", "HTTP RESPONSE:" + TResult);
            Log.d("[GudangGaram]", "HTTP LENGTH:" + Integer.toString(TResult.length()));

            if(TResult.equals("S")){
                SentResponse = "Send Data Success";
                dbHelper.delAsset(params[0]);
                Log.d("[GudangGaram]", "DELETE ID:" + params[0]);
            }
        }
        catch (Exception ex) {
            SentResponse = ex.getMessage().toString();
            Log.d("[GudangGaram]", "Catch : " + ex.getMessage().toString());
            response = "<?xml version='1.0' encoding='utf-8'?><Message response='FAIL' ErrorMessage='The System is under maintenance or having some techical issue.'></Message>";
        }
        Log.d("[GudangGaram]", "SoapAccessTaskAsync :: end " +  params[3] + " doInBackground");

        return "";
    }
}
