package com.gudanggaramtbk.amscanner;

import android.app.Dialog;
import android.app.LauncherActivity;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Camera;
import android.net.wifi.p2p.WifiP2pManager;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import com.google.zxing.client.android.Intents;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.Settings.Secure;
import static java.security.AccessController.getContext;


public class ScanAssetActivity extends AppCompatActivity implements OnClickListener {
    private Button CmdScanAsset, CmdSave, CmdCancelAll, CmdAddAsset, CmdDelAsset;
    private TextView TxtScanAsset, TxtScanQty, lblRoom, lblEmployee;
    private Boolean ScanState;
    private ListView lv;
    private List<String> lvi;
    private ArrayAdapter<String> ad;
    private MySQLiteHelper ds;
    private SQLiteDatabase sdb;
    private String DeviceID;
    ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>(2);
    private SimpleAdapter adapter;
    private int xposition;
    private SharedPreferences config;
    private Timer timer1,timer2;
    private AlertDialog alert;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_asset);

        CmdScanAsset = (Button)findViewById(R.id.cmd_ScanAsset);
        CmdCancelAll = (Button)findViewById(R.id.cmd_cancel);
        CmdSave = (Button)findViewById(R.id.cmd_save);
        CmdAddAsset = (Button)findViewById(R.id.cmd_addAsset);
        CmdDelAsset = (Button)findViewById(R.id.cmd_delAsset);





        lblRoom = (TextView) findViewById(R.id.lbl_SCRoomCode);
        lblEmployee = (TextView) findViewById(R.id.lbl_SCEmployeeCode);
        TxtScanAsset  = (TextView) findViewById(R.id.txt_ScanAsset);
        TxtScanQty = (TextView) findViewById(R.id.txt_ScanQty);


        config = getSharedPreferences("AMSCannerSetting", MODE_PRIVATE);
        ScanState = config.getBoolean("ScanMode",false);

        if(ScanState == Boolean.TRUE){
            CmdScanAsset.setVisibility(View.VISIBLE);
            CmdAddAsset.setVisibility(View.VISIBLE);
        }
        else{
            CmdScanAsset.setVisibility(View.INVISIBLE);
            CmdScanAsset.setVisibility(View.GONE);
            CmdAddAsset.setVisibility(View.INVISIBLE);
            CmdAddAsset.setVisibility(View.GONE);
        }


        TxtScanQty.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) {
                // set focus to txtScanAsset
                timer1 = new Timer();
                timer1.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if(TxtScanQty.getText().length() > 0){

                                    TxtScanQty.clearFocus();
                                    TxtScanAsset.requestFocus();
                                    TxtScanAsset.setCursorVisible(true);
                                }
                            }
                        });
                    }

                }, 600);
            }
        });

        TxtScanAsset.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                if(ScanState == Boolean.FALSE) {
                    timer2 = new Timer();
                    timer2.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    //Toast.makeText(getApplicationContext(),TxtScanAsset.getText().toString(), Toast.LENGTH_LONG).show();
                                    EH_cmd_add_asset(TxtScanAsset.getText().toString(),TxtScanQty.getText().toString());
                                }
                            });
                        }

                    }, 600);
                }
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
               if(ScanState == Boolean.FALSE){
                   // user is typing: reset already started timer (if existing)
                   if (timer2 != null)
                       timer2.cancel();
               }
            }
        });

        // change focus to Qty Scan by Default
        TxtScanQty.requestFocus();
        TxtScanQty.setCursorVisible(true);

        xposition = 0;

        ds = new MySQLiteHelper(this);
        // open database file
        sdb = ds.getWritableDatabase();
        // add button listener
        lv = (ListView) findViewById(R.id.lst_asset);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                xposition = position;
                //aList9.remove(aList9.size() - 1);
                //adapter.notifyDataSetChanged();
            }
        });


        HashMap<String, String> map;
        String[] from = {"AssetCode","ScanQty"};
        int[] to = {android.R.id.text1, android.R.id.text2};

        adapter = new SimpleAdapter(this, list, android.R.layout.simple_list_item_2, from, to);
        lv.setAdapter(adapter);

        DeviceID =  Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
        lblRoom.setText(getIntent().getStringExtra("ROOM_CODE").toString());
        lblEmployee.setText(getIntent().getStringExtra("EMPLOYEE_CODE").toString());

        CmdScanAsset.setOnClickListener(this);
        CmdSave.setOnClickListener(this);
        CmdAddAsset.setOnClickListener(this);
        CmdDelAsset.setOnClickListener(this);
        CmdCancelAll.setOnClickListener(this);

        /*
        CmdCancelAll.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                onCancelClick();
            }
        });
        CmdSave.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                onSaveClick();
            }
        });
        */


    }

    @Override
    public void onClick(View v){
        int SummonID;
        long RecordId;
        MainActivity ma;

        SummonID = v.getId();
        if(SummonID == R.id.cmd_ScanAsset){
            EH_cmd_scan();
        }
        else if(SummonID == R.id.cmd_save){
            EH_cmd_save();
        }
        else if(SummonID == R.id.cmd_cancel){
           // EH_cmd_clear();
           // Toast.makeText(getApplicationContext(),"Clear All", Toast.LENGTH_LONG).show();
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Clear Confirm");
            builder.setMessage("Function Clear akan menghapus semua list data asset stock opname. Apakah anda yakin ?");
            builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    EH_cmd_clear();
                    dialog.dismiss();
                }
            });
            builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            AlertDialog alert = builder.create();
            alert.show();
        }
        else if(SummonID == R.id.cmd_addAsset) {
            EH_cmd_add_asset(TxtScanAsset.getText().toString(), TxtScanQty.getText().toString());
        }
        else if(SummonID == R.id.cmd_delAsset) {
            EH_cmd_delete();
        }
    }

    public void EH_cmd_clear(){
            list.removeAll(list);
            adapter.notifyDataSetChanged();
    }

    public void EH_cmd_delete(){
        if(list.isEmpty() == false) {
            list.remove(xposition);
            adapter.notifyDataSetChanged();
            Toast.makeText(getApplicationContext(),"List Deleted", Toast.LENGTH_LONG).show();
        }
        else{
            Toast.makeText(getApplicationContext(),"List Already Empty !", Toast.LENGTH_LONG).show();
        }
    }

    public void EH_cmd_scan(){
        IntentIntegrator SI_Scan_asset;
        Toast.makeText(getApplicationContext(),"Scan Asset", Toast.LENGTH_LONG).show();

        TxtScanAsset.setFocusable(true);
        TxtScanAsset.requestFocus();

        SI_Scan_asset = new IntentIntegrator(this);
        SI_Scan_asset.setCaptureActivity(CaptureCustomeActivity.class);
        SI_Scan_asset.setBeepEnabled(false);
        SI_Scan_asset.initiateScan();
    }

    public void EH_cmd_add_asset(String P_AssetCode, String P_Qty){
        HashMap<String, String> map;

        if(TxtScanAsset.getText().toString().length() > 0 && TxtScanQty.getText().toString().length() > 0) {
            //lvi.add(TxtScanAsset.getText().toString());
            map = new HashMap<String, String>();
            map.put("AssetCode", P_AssetCode);
            map.put("ScanQty", P_Qty);
            list.add(map);

            adapter.notifyDataSetChanged();
            TxtScanAsset.setText("");
            TxtScanQty.setText("");

            // focus on txtScanQty
            TxtScanAsset.clearFocus();
            TxtScanQty.requestFocus();
            TxtScanQty.setCursorVisible(true);
            //Toast.makeText(getApplicationContext(),"Asset Code Added", Toast.LENGTH_LONG).show();
        }
    }

    public void EH_cmd_save(){
        for (HashMap<String, String> ohp : list) {
            ds.addAsset(lblRoom.getText().toString(),
                    lblEmployee.getText().toString(),
                    ohp.get("AssetCode").toString(),
                    DeviceID.toString(),
                    ohp.get("ScanQty").toString());
        /*
        for (String AssetCodeItem: lvi) {
            ds.addAsset(lblRoom.getText().toString(),
                    lblEmployee.getText().toString(),
                    AssetCodeItem,
                    DeviceID.toString(),
                    TxtScanQty.getText().toString());
        }
        */
            if (list.size() > 0) {
                Toast.makeText(getApplicationContext(), "Successfully saved!", Toast.LENGTH_LONG).show();
                setResult(RESULT_OK);
                finish();
                // back to main screen
                Intent IMain = new Intent(ScanAssetActivity.this, MainActivity.class);
                IMain.putExtra("REFRESH", "Y");
                startActivity(IMain);
            } else {
                setResult(RESULT_CANCELED);
                Toast.makeText(getApplicationContext(), "Failed to save!", Toast.LENGTH_LONG).show();
            }
        }
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        IntentResult SR_Asset;
        String StrScanAsset;

        switch(requestCode) {
            case IntentIntegrator.REQUEST_CODE:
            {
                if (resultCode == RESULT_CANCELED){
                }
                else
                {
                    SR_Asset = IntentIntegrator.parseActivityResult(requestCode,resultCode,intent);
                    if(SR_Asset != null){
                        StrScanAsset = SR_Asset.getContents();
                        TxtScanAsset.setText(StrScanAsset);
                    }
                }
                break;
            }
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ds.close();
    }


}
