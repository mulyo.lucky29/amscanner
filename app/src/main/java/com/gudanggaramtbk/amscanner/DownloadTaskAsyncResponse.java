package com.gudanggaramtbk.amscanner;

/**
 * Created by LuckyM on 6/10/2017.
 */

public interface DownloadTaskAsyncResponse {
    void PostDownloadAction(String output);
}
