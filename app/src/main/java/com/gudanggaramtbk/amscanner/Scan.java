package com.gudanggaramtbk.amscanner;

import java.sql.SQLDataException;

/**
 * Created by luckym on 5/29/2017.
 */

public class Scan {
    private String RoomCode;
    private String EmployeeCode;
    private String AssetCode;
    private String DeviceID;
    //private int ID;
    private String ID;
    private String Flag;
    private String Qty;
    private String ScanDate;

    public Scan(){

    }

    public Scan(String PID, String PRoomCode, String PEmployeeCode, String PAssetCode, String PQty, String PScanDate){
        this.ID = PID;
        this.EmployeeCode = PEmployeeCode;
        this.RoomCode = PRoomCode;
        this.AssetCode = PAssetCode;
        this.ScanDate = PScanDate;
        this.Qty = PQty;
    }

    // getter
    public String getRoomCode() {
        return RoomCode;
    }
    public String getEmployeeCode() {
        return EmployeeCode;
    }
    public String getAssetCode() {
        return AssetCode;
    }
    public String getDeviceID() {
        return DeviceID;
    }

    //public int getID() { return ID; }
    public String getID() { return ID; }
    public String getFlag() { return Flag; }
    public String getScanDate() { return ScanDate; }
    public String getQty() { return Qty; }

    // setter
    public void setRoomCode(String SRoomCode) {
        this.RoomCode = SRoomCode;
    }
    public void setEmployeeCode(String SEmloyeeCode) {
        this.EmployeeCode = SEmloyeeCode;
    }
    public void setAssetCode(String SAssetCode) {
        this.AssetCode = SAssetCode;
    }
    public void setDeviceID(String SDeviceID) {
        this.DeviceID = SDeviceID;
    }
    //public void setID(int SID) { this.ID = SID; }
    public void setID(String SID) { this.ID = SID; }
    public void setFlag(String SFlag) { this.Flag = SFlag; }
    public void setScanDate(String SScanDate){ this.ScanDate = SScanDate; }
    public void setQty(String SQty) { this.Qty = SQty; }

}
